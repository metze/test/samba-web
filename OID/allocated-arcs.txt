!===========================================================================================
!==
!==	Allocated Arcs from the Samba Team Private Enterprise Number
!==	ISO(1) org(3) dod(6) internet(1) private(4) enterprise(1) Samba(7165)
!==	
!==	Arc allocation is maintained by jerry carter <jerry@samba.org>.  Please notify
!==	me if you need an OID and update this file.
!==
!===========================================================================================

ARC	Owner			Contact		     Purpose
---	-----			-------	             -------
.1	Plainjoe.org		jerry@samba.org	     Use for Plainjoe.org domain
                                                     and examples in O'Reilly LDAP book
.2	Samba Team              jerry@samba.org      schema for representing smbpasswd
.3	Jean-Francois.Micouleau@dalalu.fr            Experimental SNMP fun
.4      Samba Team              jerry@samba.org      Samba4 LDAP schema 
