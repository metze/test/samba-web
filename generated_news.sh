#!/bin/bash
#

LC_ALL=C
export LC_ALL
LANG=C
export LANG
LANGUAGE=C
export LANGUAGE

set -u
set -e
umask 0022

generate() {
	local dst="$1"
	local count="$2"
	local filter="$3"

	test "${dst}" -nt "posted_news/" && {
		echo "${dst}: up to date"
		return 0
	}

	echo "${dst}: regenerating"
	files=$(find posted_news/ -type f -name "${filter}" -printf "%f\n" | sort -r | head -${count} | xargs)
	{
		for f in ${files}; do
			echo "<!--#include virtual=\"/samba/posted_news/${f}\" -->"
		done
	} > ${dst}

	return 0
}

generate_latest_stable_release() {
	local dst="$1"
	local download_url="$2"

	pushd history >/dev/null
	ALL_VERSIONS=$(ls samba-*.html | cut -d '-' -f2- | cut -d '.' -f1-3 | sort -t. -k 1,1n -k 2,2n -k 3,3n -u)
	LATEST_VERSION=$(echo "${ALL_VERSIONS}" | tail -1)
	popd >/dev/null

	echo "LATEST_VERSION: ${LATEST_VERSION}"

	local tgz="samba-${LATEST_VERSION}.tar.gz"
	local asc="samba-${LATEST_VERSION}.tar.asc"
	local release_notes="history/samba-${LATEST_VERSION}.html"

	test "${dst}" -nt "${release_notes}" && {
		echo "${dst}: up to date"
		return 0
	}

	echo "${dst}: regenerating"
	{
		echo "<!-- BEGIN: ${dst} -->"
		echo "<p>"
		echo "<a href=\"${download_url}/${tgz}\">Samba ${LATEST_VERSION} (gzipped)</a><br>"
		echo "<a href=\"/samba/${release_notes}\">Release Notes</a> &middot;"
		echo "<a href=\"${download_url}/${asc}\">Signature</a>"
		echo "</p>"
		echo "<!-- END: ${dst} -->"

	} > ${dst}.tmp
	mv ${dst}.tmp ${dst}
}

generate "generated_news/latest_10_headlines.html" "10" "*.headline.html"
generate "generated_news/latest_10_bodies.html" "10" "*.body.html"
generate "generated_news/latest_2_bodies.html" "2" "*.body.html"

download_url="https://download.samba.org/pub/samba/stable"
generate_latest_stable_release "generated_news/latest_stable_release.html" "${download_url}"
